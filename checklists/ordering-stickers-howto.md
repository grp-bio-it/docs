---
title: "HOWTO: Order Stickers"
layout: page
permalink: /checklists/stickers/
parent: Checklists
tags: ["marketing", "outreach", "community building", "acknowledgment"]
---

Stickers are useful for promotion of the Bio-IT Project,
and a small token of gratitude to those who contribute to the community.

In the past, Bio-IT has ordered "hex" stickers (for laptops etc)
from Sticker Mule.
However, we now use [Camaloon][camaloon].
The process is as follows:

- save your sticker design in a vector graphic format (SVG, PDF)
- log into site
- upload design
- order stickers

{% include links.md %}
