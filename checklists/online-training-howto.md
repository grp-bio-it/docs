---
title: "HOWTO: Teaching via Live Stream"
layout: page
permalink: /checklists/online-teaching/
parent: Checklists
tags: ["online", "training"]
---

This list is intended to supplement
the checklists
[for internal training events]({{site.baseurl}}checklists/course-internal/)
and [external training events]({{site.baseurl}}checklists/course-external/)

## Before the course

- [ ] Make sure that venue has video conferencing infrastructure
- [ ] Give notice to Photolab of intention to use video conferencing facilities - they will provide connection details to share with remote participants
- [ ] Create a separate "Remote Attendance" ticket type for event
- [ ] Send connection details to participants who signed up to attend remotely
- [ ] Remind remote participants to keep their microphones muted when joining the call
- [ ] Prepare feedback survey with two tracks, to identify differences in feedback from online and in-person participants
- [ ] Create/choose a channel on the [EMBL Chat][embl-chat] as a place for participants to ask questions/discuss course content/share URLs
- [ ] (Optional) create a shared notetaking document ([CryptPad][cryptpad], [GoogleDoc][gdoc], [HackMD][hackmd], etc) -  this can be helpful e.g. when pairing up georgraphically-dispersed participants for exercises

## During the course

- [ ] Arrive early to allow time for additional technical setup
- [ ] Ask a co-instructor/helper/confident participant to tell instructor when a question is asked in the chat channel
- [ ] Always wear a microphone when teaching
- [ ] Instructor should remember to repeat questions & comments from local participants (not miced up) for the benefit of those following remotely
- [ ] Remember not to rely on laser pointer when teaching

{% include links.md %}
