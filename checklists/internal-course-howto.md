---
title: "HOWTO: Internal Training"
layout: page
permalink: /checklists/course-internal/
parent: Checklists
tags: ["in-person", "online", "training", "events"]
---

## Before the course

- [ ] Identify all the potential trainers
- [ ] Define the general structure/content of the course
- [ ] List system requirements for the course
- [ ] List prerequisites/assumed prior knowledge for the course
- [ ] Check the availability of the trainers and preferred venue(s)
- [ ] Create a calendar entry and book the venue for the decided dates
- [ ] Invite instructors to the calendar event so they receive reminders and updates
- [ ] Invite [EMBL Heidelberg - Career development for scientists][embl-careers-calendar] calendar so that course shows up on career development intranet pages
- [ ] Set-up a course registration page on the Bio-IT Portal
- [ ] Advertise course to Bio-IT Announce mailing list
- [ ] Advertise course via EMBL-HD Unit Secretaries
- [ ] Create a waiting list if the course is oversubscribed
- [ ] Organize meetings with the trainers to finalize the course materials
- [ ] Set up a git repository to compile and share the course materials
- [ ] Create a post-workshop survey with Bio-IT SurveyMonkey account
- [ ] Create a certificate template for the participants
- [ ] Order catering through the relevant intranet pages
- [ ] Send an announcement email
- [ ] Send request for technical setup
- [ ] Collect keys and check the systems a day before
- [ ] Gather all the teaching materials (sticky notes, boards, pens, pointers, projector etc.)
- [ ] Send a reminder emails to the participants
- [ ] Offer spaces as they become free to participants from the waiting list

## After the course

- [ ] Send a 'thank you email' with the link to the survey to the participants and the trainers
- [ ] Send a digital version of the certificates on request
- [ ] Gather any important feedback for the course
- [ ] Add attendance and key feedback details to [our course tracking data][bio-it-training-repo] (internal access only)
- [ ] organise a debriefing session for the instructors, where feedback and planned changes can be discussed
- [ ] create Issues, relating to changes required, on GitLab repository for course material (or a list if no such repository exists)
- [ ] follow up on listed issues/tasks as soon as possible after the course

## See also

- Organising an external course? See the additional items in the [External Courses checklist]({{site.baseurl}}checklists/course-external)
- Providing a live stream connection to your course? See the additional items in the [Teaching via Live Stream checklist]({{site.baseurl}}checklists/online-teaching/)

{% include links.md %}
