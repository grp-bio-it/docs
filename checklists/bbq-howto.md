---
title: "HOWTO: Barbecue @EMBL"
layout: page
permalink: /checklists/bbq/
parent: Checklists
tags: ["in-person", "social", "casual", "community building", "acknowledgment", "events"]
---

## Preparation

### Booking the Grillplatz

To book the EMBL Grillplatz, contact Facility Management. In the summer, when the Grillplatz is most popular, you will need to do this pretty far in advance. She will tell you the rules: basically, they provide the handle for operating the grill crank and a (pretty good!) JBL Bluetooth speaker but you have to arrange everything else yourself. Susan will take a €100 deposit for the crank handle. You will get the money back when you return it.

### Catering

To place an order for food & drink, contact EMBL Catering by email with details about what you want, an estimate of how many people you will be feeding, when the event will be happening, and the budget number that the costs should be charged to. To cater well for vegans or those with other dietary requirements/preferences, you may need to organise appropriate supplies yourself.
As well as the food & drinks, catering will provide knives & forks, glasses, a bottle opener, and a bread knife.

### What you need to bring

Assuming that you have ordered food and drink via the catering team, you will need to arrange the following yourself:

- [ ] charcoal, lighter fluid, and matches. I recommend 12 kg of charcoal for a BBQ for ~50 people. You can buy charcoal from the catering team, but they only offer 20 kg bags.
- [ ] paper plates & napkins.
- [ ] an oven mitt/glove. I recommend a one-handed thing, not the two-handed joined together ones, and ideally one that you don't mind getting ruined by the soot and general BBQ filth.
- [ ] a music playing device 🤘 Don't forget an AUX cable if you are too old-fashioned to use Bluetooth...
- [ ] hot sauce 🌶 Catering will provide ketchup, mayo, and mustard.
- [ ] some boxes to take leftover food home with you - catering will have to throw out anything that you return ☹
- [ ] some crushed ice and a container, if you want to keep drinks cool.
- [ ] some simple cleaning supplies for wiping down surfaces after you're done.


## On the day

- [ ] find some friendly volunteers to help you - it's a lot to do on your own!
- [ ] get started with preparing ~2 hours before the time that you've asked people to arrive at.
- [ ] get the crank handle and speaker from Susan in Facility Management. It goes in the hole in the mechanism next to the lightswitch on one of the wooden uprights/pillars.
- [ ] light the coals ~90 min before you want to start eating. Use all the coal - it might seem like a lot but it's a big grill and you will need it.
- [ ] pick up the catering from the cafeteria. The best route from there back to the Grillplatz is via the elevator next to stores to the fifth floor, turn right out of there and go through the corridors next to the ALMF. Be careful carrying the trolleys etc down the step at the door! ⚠
- [ ] have fun! 🎉 🥂
- [ ] once you're done, tidy up the area, make sure that any leftover food is back in the big yellow crates that catering provide - anything left out will attract animals - but try to take as much leftover food home with you as possible. Catering will have to thrown out anything you give back to them!

## After the event

- [ ] give the catering stuff back. Bottles, crates, glasses, bottle opener, all go back to the cafeteria. The yellow crates, knives and forks, other stuff that needs washing up, all goes to the canteen. To get to the canteen, I recommend taking the same elevator as before back to floor 2, then heading towards Genome Biology and taking the other elevator down to floor 1, through the biocomputing corridor and outside.
- [ ] clean the surfaces of the grill and tables etc. Make sure that there's no broken glass on the floor etc and generally get the Grillplatz looking like it did before your event.
- [ ] give the crank handle and speaker back to Susan and collect your €100 💰

_This page adapted from an entry on the Zeller Team wiki._

{% include links.md %}
