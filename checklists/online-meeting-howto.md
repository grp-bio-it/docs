---
title: "HOWTO: Online Meeting"
layout: page
permalink: /checklists/online-meeting/
parent: Checklists
tags: ["online", "events"]
---

## Before the meeting

- [ ] Choose your video conferencing platform. Bio-IT has a [Zoom](https://zoom.us) account. Based on experience with many different platforms, theirs appears to be the one most able to cope with large numbers of participants, screen-sharing, low bandwidth, etc.
- [ ] share the URL for the meeting with all participants
- [ ] set up a shared notetaking document ([CryptPad][cryptpad], [GoogleDoc][gdoc], [HackMD][hackmd], etc)

## During the meeting

- [ ] try to wear a headset, if possible. Especially if calling from a laptop that you will also be typing on during the call...
- [ ] start by reminding everyone to keep their microphone muted when they're not speaking to minimise background noise. This also helps to reduce the number of times that people will be interrupted.
- [ ] ask people to raise their hand in the camera if they'd like to speak. If they'd prefer not to do this, agree on something for them to type into the chat window (e.g. 'hand') instead.
- [ ] I recommend switching to "gallery" view (lots of smaller pictures of everyone instead of one big one of whoever is currently speaking), as this gives you a better chance of keeping track of who wants to speak and whose turn it is next.
- [ ] encourage everyone on the call to contribute to notetaking, but also try to find one specific person who will be responsible for these or it probably won't happen...
- [ ] if necessary as chair, give everyone an opportunity to introduce themselves. This can eat a lot of time, so encourage everyone to be brief e.g. by giving them specific points to address in their introduction ("Describe your work in three words", etc).
- [ ] make sure that you finish on time.

## After the meeting

- [ ] if necessary, share the notes with everyone who attended and/or make them available for people to find in the future.
- [ ] you can set up Zoom to export the chat and any recording that you made during the call. However, these are not stored anywhere very helpful by default - take the opportunity to save them somewhere with a name and location that you'll be able to find more easily in the future.
- [ ] chairing online meetings isn't easy and it can be easy to let time get away from you. Take a moment to reflect on what went well and what you could do to improve the experience for yourself and the other participants next time.

{% include links.md %}
