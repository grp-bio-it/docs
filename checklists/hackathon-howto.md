---
title: "HOWTO: CollaborationFest @EMBL"
layout: page
permalink: /checklists/cofest/
parent: Checklists
tags: ["in-person", "online", "social", "collaboration", "community building", "training", "events"]
---

## Before the event

- [ ] Book a room (ATC Courtyard would be good)
  - it must have a projector
  - ideally with at least one whiteboard and flip chart/pens
  - make sure it's big enough. In 2019, we used room 518 and had 16 attendees. We had *just* enough space but would have benefitted from more.
- [ ] Get some multi-socket power adapters
- [ ] Create a GitLab repository to collect the project pitches as Issues
- [ ] Create an event webpage on https://bio-it.embl.de to collect the registrations
- [ ] (optional but recommended) Create a webpage to provide details of event and projects - this will keep the registration page from getting too crowded. (can re-use the repo/site from 2019: https://grp-bio-it.embl-community.io/hackathons/)
- [ ] Put out a call for project pitches
  - in 2019, we held a Zoom call around one month before the event, for people to come and pitch their project ideas
- [ ] Add projects to the hackathon webpage(s) and registration form
- [ ] Send email announcement/reminders
- [ ] Order catering for the event

## During the event

- [ ]

## After the event

- [ ]

Location, catering, power sockets/strips, white board, flip charts, sticky notes, pens, projector for the report out
Website, repo/document and call for project pitches, Post event Survey
What we did right?
Create a collaborative document (we used HackMD, we can use GitHub issues)
Pitch idea beforehand
Creating a GitHub repo, registration page and poster
Reaching out actively to members who could contribute but hadn’t registered
Creating a rough schedule
Reserving a location that is away from the workplace (still in the building)
Ordering food and drinks


Things to do differently next time
Bigger room
More coffee, more food!
Announce with more notice (allow time for people to form groups to work together on an idea)
Have a pre-event survey (maybe useful to attract more people)
Call it CoFest or something along that line to allow people to come and contribute
Reach out to more non-scientific members of EMBL for more project ideas
Have a bell for merge requests

{% include links.md %}
