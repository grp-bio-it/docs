---
title: "HOWTO: External Training"
layout: page
permalink: /checklists/course-external/
parent: Checklists
tags: ["in-person", "training", "events"]
---

This list is intended to supplement
[the checklist for internal training events]({{site.baseurl}}checklists/course-internal/)

## Registration

- [ ] For de.NBI courses, ask participants for "country of employment - more specifically Germany or non-Germany" and "Academic level (non-academic, undergrad, grad, postdoc, PI)".

## Before the course

- [ ] Budget and cost calculation (set a budget number)
- [ ] Create a template for invoices (check with Accounts (Finance))
- [ ] Setup externally-accessible course website
- [ ] Give 2 contacts on the website for any query or emergency
- [ ] Advertise in the external spaces (de.NBI, ELIXIR, Twitter etc.)
- [ ] Check accommodation for participants
- [ ] Book transportation
- [ ] Book catering or charge a budget card from petty cash to pay for canteen lunch
- [ ] (Optional) organize a conference dinner
- [ ] Print teaching materials, name badges and certificates (Photolab)
- [ ] Coordinate with external trainers
- [ ] Collect all materials from Photolab
- [ ] Send an email to the canteen to book table, and logo of the badge (identity for payment at the counter)
- [ ] Organize lanyard or name tags (with the logo for lunch payment)
- [ ] Send reminder to the trainers
- [ ] Organize power sockets, laptop, additional technical requirements (discuss with Facility Management, IT Services)
- [ ] (If necessary) select participants for the course
- [ ] Select awardees for travel grant and/or registration waiver
- [ ] Notify selected participants and send the invoices
- [ ] Notify participants of final schedule

## During course

- [ ] Keep track of number of registrations vs participants (i.e. non-shows) - also for de.NBI purposes

{% include links.md %}
