---
title: "HOWTO: Bio-IT Taskforce Meeting"
layout: page
permalink: /checklists/taskforce-meeting/
parent: Checklists
tags: ["in-person", "events", "community building", "strategy"]
---

## Before the meeting

- [ ] send an email reminder to Taskforce mailing list
- [ ] [order catering][embl-catering] (internal access only).
- [ ] book a meeting room. These meetings usually take place in the Biocomputing Seminar Room (room 100) and the bookings are usually made for all the meetings in a year at once.
- [ ] make sure that you have a chair and someone to take notes - it's difficult to do both successfully.
- [ ] ensure that you have enough sticky notes and pens for the meeting...
- [ ] pick up catering from the cafeteria

## During the meeting

- Bio-IT Taskforce meetings are traditionally carried out in [Lean Coffee][lean-coffee] format. Briefly, this means
  - at the start of the meeting everyone has a chance to give a summary (one or two sentences) of a topic they'd like to raise for discussion. Each of these points is written onto a sticky note.
  - every attendee is given a pen or two small stickers (e.g. sticky dots), which they use to vote for the topics that they'd like to discuss.
  - the sticky notes with topics are stuck to a wall and the attendees cast their votes - they can vote twice for one topic or split their votes across two topics.
  - after voting is complete, the votes are counted and the sticky notes arranged in order from most to least votes. This defines the order in which the topics will be discussed.
  - a timer is set (e.g. 6 minutes) and the first topic is discussed for that time. After the time is up, the group votes on whether or not to give more time to the topic (e.g. 4 minutes). If discussion is still ongoing after this second time period expires, the participants are asked to arrange a separate meeting at which they can continue their discussion after the meeting. (It's helpful to have a timer prominently visible, e.g. on the projector screen.)
  - this process is repeated either until all topics have been discussed or until the time allocated for the meeting has expired, whchever comes first.
- the notetaker should record the meeting attendees, the key points that were discussed, and any actions that arose from the discussions.
- the meeting chair should do their best to ensure that everyone has an equal chance to contribute to the discussion.

## After the meeting

- [ ] minutes from the meeting should be uploaded to [the relevant repository][bio-it-taskforce-minutes].
- [ ] where appropriate, the chair of the meeting should contact those who were assigned actions based on discussion at the meeting, and coordinate these actions with them.

{% include links.md %}
