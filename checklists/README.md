---
title: Checklists
layout: page
permalink: /checklists/
has_children: true
nav_order: 4
---

This is a collection of checklists and guides
used within the Bio-IT community.
These lists are useful when organising/running events
and taking care of various administrative and promotional tasks.

TODO: beer sessions; seminars; lunches
