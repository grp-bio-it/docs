---
title: "HOWTO: Bio-IT Lunch @EMBL"
layout: page
permalink: /checklists/bioit_lunch/
parent: Checklists
tags: ["in-person", "social", "casual", "community building", "acknowledgment", "events"]
---

## About

Bio-IT lunches are traditionally used to introduce new community members or discuss topics that require a faster turnaround than Taskforce meetings allow.

## Before the day

- [ ] Usually run on the 4th Wednesday of every month but not a strict rule, so other days are fine
- [ ] Send an email to invite Bio-IT core support members - see also `Bio-IT internal kanban issue #11`
- [ ] Send an email to `catering@embl.de` requesting a table (indicate **start time**, **estimated number of people** and **label on the reservation marker**)

## On the day

Usually people gather at the entrance of the EMBL Heidelberg canteen around the specified start time.  
Otherwise, look for the *Bio-IT Lunch* table reservation.


{% include links.md %}
