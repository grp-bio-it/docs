#! /usr/bin/env python
'''
Adds the 'modified_date' field to the YAML header of
all Markdown files listed in _timestamp_files.txt.
The field is populated with the date (in YYYY-MM-DD format)
of the most recent commit that included each file.

This script is intended for use in the Continuous Integration
pipeline before the pages are built.
'''


from sys import argv, stderr
import subprocess

def get_last_modified_date(markdown_file):
    '''Extract the date of the most recent commit
    that included changes to markdown_file.'''
    git_log_cmd = "git log --pretty='format:%ai' {} | head -1"
    date_string = subprocess.check_output(git_log_cmd.format(markdown_file), shell=True).strip()
    return date_string

for filename in argv[1:]:
    try:
        with open(filename, 'r') as infh:
            lines = infh.readlines()
            assert lines[0].startswith('---'), "Error: {} doesn't appear to be a valid Markdown file with a YAML header. Please check you filenames and try again.".format(filename)
            last_modified = get_last_modified_date(filename)
            lines.insert(1, "last_modified_date: '{}'\n".format(last_modified))
            with open('{}.modified'.format(filename), 'w') as outfh:
                for line in lines:
                    outfh.write(line)
        subprocess.call('mv {0}.modified {0}'.format(filename), shell=True)
    except IOError:
        stderr.write('ignoring {} as no file exists with that name')
        pass
