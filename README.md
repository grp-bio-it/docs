# Bio-IT Community Docs

<a class="navigation-list-item active" href="https://git.embl.de/grp-bio-it/docs/pipelines/">![build status](https://git.embl.de/grp-bio-it/docs/badges/master/pipeline.svg?style=flat)</a>
<a class="navigation-list-item active" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="This work is licensed under a Creative Commons Attribution 4.0 International License" src="https://mirrors.creativecommons.org/presskit/buttons/80x15/png/by.png" style="height: 20px;margin-left: 20px"/></a>

Documentation of the [EMBL Bio-IT Project][bio-it-homepage].
Bio-IT was established to build and support the computational biology/bioinformatics
community at the [European Molecular Biology Laboratory (EMBL)][embl-homepage], Heidelberg, Germany.

See the site at [`grp-bio-it.embl-community.io/docs/`][docs].

These docs provide information and practical guides about the community, the project,
and their activities.
The site was originally developed by [Toby Hodges][toby-homepage]
under the [Center for Scientific Collaboration and Community Engagement's][cscce]
concept of a *Community Playbook*, as part of the
[CSCCE Community Engagement Fellows Program][cefp-homepage] 2019 (CEFP2019).

We use `toc.html` taken from
[this amazing repo from GitHub user _allejo_](https://github.com/allejo/jekyll-toc)
under an MIT license to generate in-page table of contents.

## Build info

The [site][docs] is built with [Jekyll][jekyll]
and [GitLab Pages][gitlab-pages-docs],
using the [Just the Docs][just-the-docs] theme.
Pages are served via Continuous Integration on the [EMBL GitLab instance][embl-gitlab].

To build these pages locally,
clone this repository,
[install Jekyll and Bundler][jekyll-installation],
then run `bundle exec jekyll build` from the project root directory.
To view/test changes locally before pushing,
run `bundle exec jekyll serve`
point your favourite web browser to http://localhost:4000/docs/.

## Site structure & links

Pages are added to the site as [Markdown][markdown] files.
To be rendered on the site,
a page must begin with *front matter*:

```yaml
---
title: Example # required
layout: default # required: change this to use a different layout for the page
description: "Documentation of the EMBL Bio-IT Community." # required
permalink: / # required: defines URL path to page
parent: Page Parent # optional: used to specify page under which current page should be listed in navigational structure
has_children: true # optional: used to define navigational structure for tables of content & menus
nav_order: 0 # optional: used to control the order in which pages are displayed in the navigational structure
tags: ["example"] # optional: provide one or more tags for the page, in an array. These tags will be used to help readers find pages/info relevant to their needs
---
```

Markdown files should also end with the `{% includes links.md %}` tag,
to ensure that links are rendered correctly (see below).
In lieu of a more elegant solution,
the CI pipeline will throw an error if this tag is not found
within the last five lines of a Markdown file in the repository
(excluding `README.md` files or those starting with `_`).

Links to external pages should be written as

```markdown
[link text][link-target]
```

with a corresponding entry added to `_includes/links.md` in the format:

```markdown
[link-target]: https://link.target/
```

To ensure correct link resolution,
links to other pages *within* the site should be written as

```markdown
[link target]({{site.baseurl}}link/target/)
```

## Timestamps

There are few things more annoying than unwittingly using outdated information.
To display a "This page was last updated on YYYY-MM-DD" timestamp to a page
(with the date corresponding to the last time that specific page was modified),
add the path to the desired page(s) to `_timestamp_files.txt`.

## Contact

If you have questions about these pages,
about our community,
or about the EMBL Bio-IT Project in general,
please contact <a href="mailto:bio-it@embl.de">bio-it@embl.de</a>.

[bio-it-homepage]: https://bio-it.embl.de
[cefp-homepage]: https://www.cscce.org/cefp/
[cscce]: https://www.cscce.org/
[docs]: https://grp-bio-it.embl.de/docs/
[embl-gitlab]: https://git.embl.de/
[embl-homepage]: https://embl.de
[gitlab-pages-docs]: https://docs.gitlab.com/ee/user/project/pages/
[jekyll]: https://jekyllrb.com/
[jekyll-installation]: https://jekyllrb.com/docs/installation/
[just-the-docs]: https://github.com/pmarsceill/just-the-docs
[markdown]: https://daringfireball.net/projects/markdown/
[toby-homepage]: https://tbyhdgs.info

{% include links.md %}
