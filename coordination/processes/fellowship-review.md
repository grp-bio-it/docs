---
title: "HOWTO: Review Outreach & Travel Fellowship Applications"
layout: page
permalink: /coordination/processes/fellowship-applications/
parent: Community Processes
grand_parent: For Community Coordinators
tags: ["gratitude", "fellowship", "training", "outreach"]
---

Community members apply for the
Bio-IT Outreach & Travel Fellowship by completing a survey.
The survey is managed via Bio-IT's [SurveyMonkey][surveymonkey] account,
with email notifications sent to alert us every time a response is received.

Each application should be reviewed according to the following criteria:

- Is there sufficient unused funding allocated to the Fellowship to cover the application?
- Has the applicant actively contributed to the Bio-IT Project in the last year?
  - this could be via teaching, developing/maintaining a resource for the community, helping run an event, writing a blogpost, answering questions online, or anything else that's benefitted the community
- Is the activity for which they are requesting financial support relevant to [the mission]({{site.baseurl}}about/mission/) of the Bio-IT Project?
- Will the activity for which they are requesting financial support benefit the applicant and the wider community?
- Has the applicant already received funding up to the annual limit for the year?

{% include links.md %}
