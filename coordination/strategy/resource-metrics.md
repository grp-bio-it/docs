---
title: "Resource Usage Metrics"
layout: page
permalink: /coordination/strategy/resource-metrics/
parent: Strategy & Reporting
grand_parent: For Community Coordinators
tags: ["metrics", "resources", "reports", "gitlab"]
---

## git.embl.de Usage

The [`stamper/gitlab-metrics`][gitlab-metrics-repo] repository
(internal access only)
includes scripts for collecting and plotting out data on usage
of [git.embl.de][embl-gitlab].

As users of [chat.embl.org][embl-chat] login via the GitLab system,
the number of users reported for [git.embl.de][embl-gitlab] is inflated.
To better represent the true number of GitLab users,
we often remove any users who are not a member of any project on the system.
However, there are clear limitations to the accuracy of this reporting:
the EMBL Git Admins don't have a systematic way to
remove old accounts after users have moved on from EMBL,
nor do we currently have a method to distinguish between
"active" and "passive" users of the system.
The latter problem could probably be solved with more sophisticated querying
of the GitLab database (e.g. based on most recent commit).
The former issue is more challenging to solve,
though perhaps possible with some kind of regular, automated sweep
of the central LDAP system to identify,
and subsequently lock the account of,
users who have since left EMBL.

If you have ideas for how to approach either of the issues noted above,
please contact the [EMBl Git Admins](mailto:git-at-embl-dot-de).

#### Most recent data (Aug 2019)

_TODO: most recent plot of git.embl.de usage_

## chat.embl.org Usage

Summary statistics of chat usage can be obtained directly from the
Mattermost System Console (accessible to administrators only).

#### Most recent data (Aug 2019)

- Total channels: 189
- Total posts: 147604
- Total active users: 655
- Monthly active users: 124
- Daily active users: 29

## Software usage

TODO?

{% include links.md %}
