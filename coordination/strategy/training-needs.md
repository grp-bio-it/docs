---
title: Training Needs of the Community
layout: page
permalink: /coordination/strategy/training-needs/
parent: Strategy & Reporting
grand_parent: For Community Coordinators
tags: []
---

The diversity of different software and methods used by community members,
the increasing prevalence of data-driven molecular biology research,
and the relatively high turnover of staff at EMBL,
combine to create high demand for regular training
in computational research skills at EMBL.
The need for computational training falls into three categories:

1. basic training in fundamental skills.
  - provides researchers with the foundational knowledge they need to learn about an apply the computational methods/tools used in their field of research.
  - topics include:
    - command line computing
    - programming
    - data management
    - cluster computing
2. intermediate/best practice training in fundamental skills
  - building on the essential skills taught in basic training courses, these courses provide more information about how to use tools more effectively and efficiently. The courses often place an emphasis on good practices for reproducible and sustainable reserach computing.
  - topics include:
    - command line computing
    - programming
    - cluster computing
    - version control
3. advanced and specialised training
  - whereas beginner- and intermediate-level training provided through the Bio-IT Project often focusses on core skills for computational research (programming, command line/cluster computing, etc), advanced training is often more specific to a particular field or software. These courses typically have a smaller audience (though there are exceptions to this), reflecting the more specialised nature of the material.
  - examples from the Bio-IT Project have included:
    - Singularity/container computing
    - machine learning
    - image processing/analysis
    - workflow management
  - additional specialised training is provided by the [EMBL Computational Centres]({{site.baseurl}}members/centres/) and several EMBL Core Facilities

### Identifying New Training Topics

The course feedback survey template used by the Bio-IT Project includes a question,
asking which other training topics the participant would like to see offered,
to help gauge interest in potential new topics
and stay abreast of developing demand.

Training on new topics often arises through in-person discussion between
project coordinators and community members:
in some cases,
community members will mention a desire for training on a particular topic
(or the coordinators will connect multiple similar requests from multiple conversations),
in others, community members will express a desire to teach a particular topic.
In both cases,
the project coordinators try to follow up on these conversations
and support the development and delivery of a training course.

Links to relevant training material are collected on the
[Course Materials][bio-it-course-materials] page of the Bio-IT Portal.
The community is encouraged to publish training material under an open license
(usually MIT or CC-BY) wherever possible.

{% include links.md %}
