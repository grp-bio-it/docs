---
title: "Bio-IT Reporting"
layout: page
permalink: /coordination/strategy/reporting/
parent: Strategy & Reporting
grand_parent: For Community Coordinators
tags: ["metrics", "reports", "meetings"]
---

## Feedback to the community

### Taskforce Meeting Minutes

Notes from the bi-monthly meetings of the [Bio-IT Taskforce]({{site.baseurl}}members/taskforce/)
are stored in [the `grp-bio-it/taskforce-minutes` repository][taskforce-minutes]
(internal access only).

A first draft of the minutes is circulated among Taskforce members
via mailing list in the days following a meeting,
and members are encouraged to make corrections/add more details.
(It can be difficult for the note-taker at the meeting to capture
the details and finer points of some technical discussions.)

### Unit Seminar

Bio-IT Project representatives team up annually with [the EMBL Centres][embl-centres]
to present a talk on their activity.
Historically, these talks have been included in the SCB Unit Seminar schedule
(Friday afternoons), using one of the Gibson or Zeller team slots,
and have been used to raise awareness of Centres/Bio-IT services and,
in the case of Bio-IT,
to signpost and acknowledge community members
for their contributions to the community.

TODO: slides from previous talk(s)

### Lab Day

In past years, Bio-IT & the EMBL Centres have presented posters at the
annual [EMBL Lab Day][lab-day].
These posters have been designed to raise awareness of
activities and services provided to the community.

## Reporting to the organisation

### Collecting Feedback

Bio-IT has a (paid) "Advantage Team" account with
[SurveyMonkey][surveymonkey],
which is used to collect feedback for all our formal courses and events.
Feedback is collected from courses to help us
measure and report the quality of our courses,
understand how we can improve our teaching and/or materials,
and plan new courses.
Feedback may be collected from other events,
such as [CollaborationFests]({{site.baseurl}}checklists/cofest/) or other events being run for the first time,
where the organisers wish to gather
suggestions for improving the next incarnation,
information for reporting,
details of projects worked on, etc,
from the participants.

### SSMAC Report

Between 2015 and the end of 2018,
Bio-IT provided short reports, summarising project activity,
to Peer Bork before the bi-monthly meetings of the EMBL Heads of Unit/SSMAC.
As well as being useful for presenting highlights to these committees,
these reports have proved helpful for tracking project activity,
acting as a browsable inventory of the history of the project and community.
Instead of individual documents for these meetings,
the report for 2019 is one large document.
It is likely that we will return to writing multiple short reports in the future,
as these proved more sustainable
- not least because it is a more manageable task to detail activity every few months
and the regular schedule helps to ensure that the task is done.

### Unit Review

Each of EMBL's research units is externally reviewed in a four-year cycle.
Due to the affiliation of Bio-IT core members with the Gibson and Zeller
Teams,
and Head of Unit Peer Bork's position as
Strategic Head of Bioinfomatics (EMBL Heidelberg),
Bio-IT Project activity is mostly reported and assessed
as part of the review of the Structural and Computational Biology (SCB) unit.

The most recent SCB Unit Review took place in 2018.
Although the review panel focuses on research activity,
their [summary report][scb-unit-review-report] mentioned the Bio-IT Project twice, noting

> The Bio-IT Programme, coordinated by the unit,
> was recognised as an original and creative approach
> in both data science training and fostering collaborations across EMBL
> and partner institutions in the area of IT.
> Panel members noted, however, that Bio-IT appeared to be understaffed.

The next review is scheduled for 2022.

{% include links.md %}
