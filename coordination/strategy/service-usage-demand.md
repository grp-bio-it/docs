---
title: "Usage & Demand of Bio-IT Services & Resources"
layout: page
permalink: /coordination/strategy/service-metrics/
parent: Strategy & Reporting
grand_parent: For Community Coordinators
tags: ["metrics", "reports"]
---

As part of the 2018 survey of bioinformatics activity at EMBL,
Group & Team Leaders (GTLs) were asked,
for a selection of services and resources provided to the community,
"Does computational research in your group benefit from the following bioinformatics support activities?"
The results from this question,
for GTLs at EMBL Heidelberg,
are displayed below.

![Usage and evaluation of services available to Bio-IT community]({{site.baseurl}}images/bio-it-service-evaluation.png)

{% include links.md %}
