---
title: Strategy & Reporting
layout: page
permalink: /coordination/strategy/
parent: For Community Coordinators
has_children: true
nav_order: 1
---

Information on previous Bio-IT Project activity,
motivation and context for project strategy,
and details of when and how activity is reported.

{% include links.md %}
