---
title: Repeating Events
layout: page
permalink: /coordination/strategy/repeating-events/
parent: Strategy & Reporting
grand_parent: For Community Coordinators
nav_order: 1
tags: ["in-person", "online", "events"]
---

A list of repeating events for the Bio-IT community.

## Annually

- Biocomputing Retreat
  - usually takes place in Feb/Mar
- Bio-IT/Centres Unit Seminar
  - date varies, set via Alena Froehlichova in January
  - usually uses one of the Zeller or Gibson Team slots
- "Thank You" Summer BBQ and Hackathon
- EMBL Lab Day
  - EMBL's internal conference
  - Bio-IT usually has a poster or a short talk at the event

## Monthly

- Taskforce Meetings
  - bi-monthly, taking place at 14:00 on the first Wednesday of odd-number months
  - see [Taskforce Meeting HOWTO]({{site.baseurl}}checklists/taskforce-meeting/) for details
- Bio-IT lunch
  - monthly, every fourth Wednesday, 12:00
  - a lunch bringing together Bio-IT coordinators, bioinformatics/computational support staff, members of IT Services, and other major contributors to the Bio-IT Project
- Training interest group lunch
  - roughly monthly
  - organised by Rachel Coulthard-Graf
  - a meeting of various parties at EMBL (EICAT/Course & Conference Office, Predoc Office, Bio-IT, ELLS, General Training & Development, etc) interested in providing/organising training at EMBL
- Bio-IT Beer Sessions
  - informal events to bring the community together
  - usually begin with a discussion/talk around a technical topic of interest to the community

## Fortnightly

- [emblr][emblr], EMBL's R User Group meeting
  - 16:00 every second Tuesday
  - see the [schedule][emblr-schedule]
- [EPUG][epug]
  - EMBL's Python User Group meeting
  - 16:00 on Tuesdays when _emblr_ doesn't take place
  - see the [schedule][epug-schedule]

## Weekly

- [Drop-in sessions][drop-in], consulting on computational biology/bioinformatics-related topics
  - 10:00 - 12:00 every Tuesday
  - take place in the EMBL cafeteria
- EMBL Git Admins meeting
  - Fridays at 10:00
  - take place only if there is something to discuss and the maintainers are available to meet

{% include links.md %}
