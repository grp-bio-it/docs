---
title: Milestones
layout: page
permalink: /coordination/strategy/milestones/
parent: Strategy & Reporting
grand_parent: For Community Coordinators
tags: ['reporting', 'strategy']
---

The following are important events that impact the activity and strategy
of the Bio-IT Project:

-  next SCB review: 2022
-  next 3-year bioinformatics@EMBL survey: 2021
-  next SCB unit seminar: 2020 (exact date TBD)
-  next "Thank You" BBQ: Summer 2020 (exact date TBD)

{% include links.md %}
