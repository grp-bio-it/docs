---
title: "Course History & Feedback"
layout: page
permalink: /coordination/strategy/course-metrics/
parent: Strategy & Reporting
grand_parent: For Community Coordinators
tags: ["metrics", "training", "reports"]
---

## Course History

The history of Bio-IT courses and workshops is listed in
[the `/grp-bio-it/bio-it-reports` repository][bio-it-reports]
(internal access only).
That file includes a summary of the feedback collected for each course.
For more about course feedback, see the page on [Reporting]({{site.baseurl}}coordination/strategy/reporting/)

TODO training infographic

{% include links.md %}
