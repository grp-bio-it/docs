---
title: For Community Coordinators
layout: page
permalink: /coordination/
has_children: true
nav_order: 2
---

These pages are designed for community coordinators,
documenting various administrative processes,
important resources for project management,
providing an archive of project activity,
and listing events and dates important in the project lifecycle.

TODO: add more links to [Useful Links](links).
