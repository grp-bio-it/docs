---
title: Useful Links
layout: page
permalink: /coordination/links/
parent: For Community Coordinators
nav_order: 4
---

## Internal

These resources contain important information about
the configuration and maintenance of various Bio-IT resources.

- [Git Admin repository][git-admin-repo]
  - internal access only
  - contains configurations and setup documentation for the internal GitLab system, Mattermost chat, and continuous integration server/runner
- [Community Documentation repository]({{site.aux_links["Source"]}})
  - the source files for this site
  - see [the page about GitLab Pages]({{site.baseurl}}coordination/platforms/gitlab/#gitlab-pages) for more information
- [Bio-IT Blogs repository][bio-it-blogs-repo]
  - see [the page about GitLab Pages]({{site.baseurl}}coordination/platforms/gitlab/#gitlab-pages) for more information

## External

These links point to various resources that we have found useful.

### Community Building

- [Resources from the Center for Scientific Collaboration and Community Engagement][cscce-resources]
- [The Carpentries Community Cookbook][carpentries-community-cookbook] -  a collection of resources and how-to guides for running activities to build local peer learning communities
- [ROpenSci Blog][ropensci-blog]

### Teaching

- [Teaching Tech Together][ttt] - [Greg Wilson's][gwilson-website] book on best practices for teaching programminga nd other computing skills. Includes [a chapter on building a community of practice][ttt-community]. Free to read online.
- [Life Science Trainers][lifescitrainers]
- [MyBinder][mybinder]

### Tools

- [CryptPad][cryptpad]: like [etherpad][etherpad], CryptPad allows collaborative note-taking, and includes an option to optimise the pad for code by including line numbers and a monospaced font.
- [HackMD][hackmd]: another collaborative authoring tool, this time set up for [Markdown][markdown]. HackMD allows you to display the raw MD, the rendered version, or both in a split display, and to save documents in a collection to refer back to in the future.
- [Time and Date][timeanddate]: convert meeting times between timezones; announce events across timezones; find out how likely it is that your collaboartors are awake right now; etc
- [Cuckoo Remote Timer][cuckoo]: a shared countdown timer, useful for remote co-working sessions and meetings. Start a countdown timer and everyone connected to the page receives a notification when the countdown ends.

{% include links.md %}
