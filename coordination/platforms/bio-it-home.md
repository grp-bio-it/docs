---
title: Bio-IT Wordpress Info
layout: page
permalink: coordination/platforms/bio-it-home/
parent: Community Platforms
grand_parent: For Community Coordinators
tags: []
---

The Bio-IT homepage, published at [https://bio-it.embl.de/][bio-it-homepage],
is the main site for information about project activity.
It was designed with the aim of providing a central location linking out
to resources and the various sources of information
relevant to computing/bioinformatics at EMBL.
The site also provides listings of upcoming Bio-IT courses and events,
hosts the homepages of the [EMBL Computational Centres][embl-centres],
and handles subscriptions to the Bio-IT and Centres mailing lists.

The front page of the site contains a table of links
to pages and other sites,
divided according to the [four objectives of the project]({{site.baseurl}}about/mission/):
Training & Consulting;
Community Building;
Information Dissemination;
and Resource Coordination.

## Wordpress

The Bio-IT homepage
(also sometimes referred to as the Bio-IT _portal_)
was originally developed by former Bio-IT community members,
Reinhard Schneider, Clemens Lakner, and Sergej Andrejev,
in the [Joomla][joomla]
content management system (CMS).
In summer 2016,
Toby Hodges updated and adapted the site
to use [Wordpress][wordpress].
In its default state,
Wordpress has a simple design entirely geared towards blogging,
which can be adapted and augmented with additional functionality
available via an enormous library of plugins.

### Plugins

Below is a list of some key plugins used on the site:

- ~~[AuthLDAP][wp-authldap]~~ - now disabled in favor of OneLogin SAML SSO
  - allows Wordpress to authenticate users via EMBL's central LDAP server
  - means that users don't need to create an account for the site and can use their normal EMBL credentials instead
  - doesn't allow EMBL-EBI personnel to login
- [OneLogin SAML/SSO][wp-onelogin-samlsso]
  - allows Wordpress to authenticate users via EMBL's central SSO/IDP server
  - allows EMBL & EMBL-EBI personnel to login
- [Events Manager][wp-events-manager]
  - manage course listings, registrations, etc
  - creates external user accounts automatically for anyone registering for an event who hasn't logged in first
  - we use the paid Pro upgrade
- [Formidable Forms][wp-formidable]
  - multi-purpose plugin for creating and managing webforms
  - used to handle mailing list subscriptions, people profiles, coding club tutorials, etc
  - we use the paid Pro version

### Theme

Like most content management systems,
Wordpress treats the content and
layout, styling, and navigation of pages separately.
The structure and style of a site is based on what
Wordpress calls a _theme_.
Just like the plugins,
there is a huge library of these themes available to use for free
(though many have paid versions with more features/options for customisation).

The theme of the Bio-IT site has been adapted from [Ribosome][wp-ribosome].
Wordpress themes operate on a model of inheritance,
where one theme (the 'child') can be based on another (the 'parent')
allowing the developer to add/change only the extra functionality
that they want without having to make a copy of the entire parent.
It's important to make this child theme,
instead of editing the parent directly,
as themes are updated often and an update will likely
result in any changes made being overwritten.

The child theme developed for the Bio-IT site is
imaginatively called 'Ribosome EMBL',
and includes changes to the styling,
additional functionality for menus,
profile photos,
custom header and footer, etc.

{% include links.md %}
