---
title: EMBL GitLab
layout: page
permalink: /coordination/platforms/gitlab/
parent: Community Platforms
grand_parent: For Community Coordinators
tags: []
---

Many members of the Bio-IT community use the local GitLab system,
at [git.embl.de][embl-gitlab],
to manage their projects.
An instance of [GitLab Community Edition][gitlab],
[git.embl.de][embl-gitlab] was initially setup by
former Bio-IT community members Holger Dinkel and Grischa Toedt.
Holger and Grischa migrated projects that were already tracked
in the preceding [gitolite][gitolite] system
(accessible by command-line only) that was hosted and administered
by Frank Thommen, another community alumnus.
The system,
which
(like [GitHub][github])
provides a graphical interface to manage projects under version control,
has been
[increasingly popular]({{site.baseurl}}coordination/strategy/resource-metrics/)
ever since.

## Mattermost

The GitLab system is linked to [chat.embl.org][embl-chat],
a workplace chat system based on [Mattermost][mattermost].
The system has many different public and private channels
for discussion around different topics and groups.

Popular public channels include those for discussion of
[EMBL's central HPC cluster][chat-cluster],
[R and statistics][chat-statistics]
and [general Bio-IT topics][chat-bio-it].

Details of the installation and configuration of this chat system
can be found in the [`grp-admin/gitadmin/`][git-admin-repo]
repository
(internal access only).

## Continuous Integration

Users of the EMBL GitLab system can set up
[continuous integration (CI)][ci-wikipedia]
for their projects,
allowing them to perform automated testing and deployment
of their software.
Notable uses of the GitLab CI system include the building and deployment of
EMBL's [central library of EasyBuild modules][software-repo],
and GitLab Pages ([see below](#gitlab-pages)).

See [this page][gitlab-ci-docs] more information on GitLab CI and how to
set up continuous integration for your projects.
Details of the installation and configuration of the shared CI runner
provided by Bio-IT
can be found in the [`grp-admin/gitadmin/`][git-admin-repo]
repository
(internal access only).

## GitLab Pages

GitLab Pages allows static webpages to be built and hosted from GitLab,
via the continuous integration system.
Pages built from projects on [git.embl.de][embl-gitlab] are
hosted under the `embl-community.io` domain e.g.
pages built from the (imaginary) `git.embl.de/username/project/` repository
will be hosted at `https://username.embl-community.io/project/`.

The building and deployment of pages can be configured via GitLab CI,
as described in detail [here][gitlab-pages-docs].
As an example, this site is built from
[this source repository]({{site.aux_links["Source"]}}).
For more information,
see also [this page on bio-it.embl.de][bio-it-gitlab-pages].

## Administration

GitLab CE,
Mattermost,
and the CI runner
are all hosted on virtual machines provided by EMBL IT Services.
Costs incurred for additional storage are covered by the Bio-IT budget.

The systems are managed by
the [EMBL Git Admins]({{site.baseurl}}members/git-admins/):
volunteers from the community
who handle user accounts,
keep software up-to-date,
perform various housekeeping tasks on the host machines,
and provide training and support.

Their notes on the setup and configuration of these systems
are kept in the [`grp-admin/gitadmin/`][git-admin-repo] repository.

{% include links.md %}
