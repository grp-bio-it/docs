#! /usr/bin/env python
'''
checks every Markdown file for the `{% include links.md %}` tag
and raises an error if it's missing
'''

from sys import argv, stderr
from glob import iglob
import subprocess

section_markdown_files = []
for filename in iglob('*/*.md'):
    if not filename.startswith('_'):
        if not filename.endswith('README.md'):
            section_markdown_files.append(filename)
for filename in iglob('*/*/*.md'):
    if not filename.startswith('_'):
        if not filename.endswith('README.md'):
            section_markdown_files.append(filename)

no_tag = []
for filename in section_markdown_files:
    cmd = r"tail -5 " + filename + r" | egrep '{% include links.md %}' > /dev/null"
    try:
        exit_code = subprocess.check_call(cmd, shell=True)
    except subprocess.CalledProcessError:
        no_tag.append(filename)
        pass
if no_tag:
    raise UserWarning("No include links.md tag was found in the last five lines of the following Markdown files:\n" + "\n".join(no_tag))
    exit(1)
