---
title: For Community Members
layout: page
permalink: /members/
has_children: true
nav_order: 2
---

The [Bio-IT Portal][bio-it-homepage] is intended to provide
most information relevant to community members,
such as [course listings][bio-it-courses]
and [materials][bio-it-course-materials],
a [guide for newcomers][bio-it-newcomers-guide],
and details of
[compute resources available to community members][bio-it-compute-info].

That site also provides everything you need to know about how to
[__get involved with the Bio-IT community__][bio-it-get-involved].

Here we provide further information that may be interesting to community
members, about various important groups within the community,
and the way that Bio-IT training is organised and delivered.

## Terminology

- The **Bio-IT Project** [was established]({{site.baseurl}}about/mission/) to build and support the computational biology/bioinformatics community at EMBL Heidelberg. Its activities now extend to all six EMBL sites.
- We refer to every computational researcher/support staff member who engages with Bio-IT Project activity as members of the **Bio-IT community**.

{% include links.md %}
