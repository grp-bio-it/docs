---
title: EPUG/emblr Session Lead Onboarding Info
layout: page
permalink: /members/epug-emblr-onboarding/
parent: For Community Members
tags: []
---

In this document,
we try to outline what's involved in running a session of
the EMBL Python User Group or _emblr_.
These are bi-weekly sessions,
usually an hour long,
providing users at EMBL a chance to meet others in the community,
discuss the languages and/or learn new skills.

## About the sessions

Most sessions take the form of a short demo or tutorial,
though the purpose of this part may differ.
For example,
previous sessions have begun with a demo/tutorial
aiming to:

- set the scene for a more general group discussion
- teach attendees how to do something with the language
- demonstrate a particular module/library
- introduce a problem the session lead would like help to solve

Sessions are typically attended by 5-20 participants,
and last ~60 minutes.

## What is involved with leading a session?

Each EPUG & emblr session is led by a member of the community who wants to
share their knowledge with others.
The session lead volunteers in advance,
discusses and agrees on the topic of their session with the session organisers,
as well as a suitable slot in the schedule for their session(s),
and provides the organisers with information about
what attendees can expect to hear about if they join the session.
This allows the organisers to announce the session in advance.

During the session,
the lead might:

- introduce the topic to be discussed (always)
- provide a demo/tutorial of the problem/solution/concept/module/library (usually)
- set exercises for the participants to complete, to test their understanding of the material (usually only in a tutorial session)
- chair the discussion (on request, this responsibility can also be assigned to another participant/organiser)
  - keep the conversation on-topic
  - ensure everyone has a chance to contribute
  - keep track of time

Here are a few things we _don't_ expect of the session lead:

- __to deal with logistics__: room booking/video conference organisation/catering/session announcements/GitLab repository
- __super-polished, super-high quality material and teaching__: these sessions are relatively informal, with the primary purpose of bringing together members of the community who'd like to compare notes and share experiences from programming
- to know enough about __Git & GitLab__ to add their materials to the relevant repository (but this is a great opportunity to get some guidance and make your first merge request!)
- __to commit to leading more than one session__: we'll be delighted if you offer to lead a session - and thrilled if you choose to come back and do it again - but there's no expectation or pressure to continue to contribute beyond the level of involvement you're comfortable with
- __to be an expert on the topic__: it's completely normal (expected, even) that the session lead doesn't know absolutely everything about their chosen topic. In fact, there's often someone else in the group with more experience/knowledge than the session lead.

## How should I prepare to lead a session?

See the checklists below for some guidance on what to do
when preparing and leading your session.

## How much time is required?

We estimate the time required to prepare and run a session
to be between half a work day and a whole work day,
depending on the format and whether you are aware of pre-existing material
that could be used.

## What are the benefits of leading a session?

Choosing to lead an EPUG/_embl_ session can be a great way to:

- get to know other users in the community
- let others get to know you and what you're working on
- figure out _what you don't know_ about the topic. The reading you might do in preparation for the session, and the (friendly) questions asked by other participants during it, can help you find new things about whatever you're presenting
- hear different perspectives on your topic
- have fun! EPUG and _emblr_ are both full of friendly, positive folks who enjoy programming and are curious to learn more. Leading a session can be a very rewarding experience and a chance to share your enthusiasm with others
- qualify for the [Bio-IT Fellowship][bio-it-fellowship-info]. Anyone who's actively contributed to the Bio-IT community is encouraged to apply for funding through our Travel and Outreach Fellowship programme. These small grants can help cover the costs of attending a conference/meeting/course or organising an event relating to the mission of the Bio-IT Project.

## Session lead checklist

### Before the session

- [ ] __Decide on the objective of the session.__ What do you want to show to people?
- [ ] __Be realistic about what you can cover.__ An hour is not a very long time. The first five minutes will be lost to people trickling in and general chit-chat. You'll probably need to spend at least ten minutes introducing the topic,
and pause often during the live demo/teaching to answer questions and get involved in deeper discussion. Our advice is: write down everything you'd like to cover in the session. Then decide what you think can be covered in an hour, before ordering those things by priority - what is it essential that you talk about during the session - and prepare to accept that you'll run out of time roughly halfway through your list of talking points. We've lost count of how many times people have run out of time before they got through all the material they prepared. And we've never had to start counting the number of times someone finished early. If you'd like a second opinion about how much content to prepare, don't hesitate to reach out to the session organisers.
- [ ] __Provide details of prerequisites.__ What are you assuming attendees will know before they arrive at the session? You can add a summary of this expected prior knowledge to the relevant scheduling sheet for [EPUG][epug-schedule] or [emblr][emblr-schedule] (contact the organisers for access to edit these documents).
- __Prepare your notes, examples, exercises in advance.__ Whether you're running a session on something you've been doing/using for years, or talking about something new, you don't want to have to think about what you want to say or come up with examples on the fly. Choosing the right example(s) can save you a lot of time and make your material much clearer. You don't need to write out exactly what you want to say, but preparing some short notes will help keep you on track, and including key lines/blocks of code can help to reduce the amount of debugging you'll have to do live. (You will probably end up debugging live anyway - more on this in the next section.)
  - Rmarkdown and Jupyter Lab/Notebooks are good formats for these notes (and for material to share with the session participants) - they allow you to combine text (e.g. section headings and notes), images, and code into a single document. Again, you don't need to include complete code blocks, but providing important boilerplate (loading in data, function definitions, extended literal definitions of data structures, etc) pre-written in a format like this saves time during the session.
- [ ] if you'd like attendees to be able to follow along through the material with you, __add material from the session to the relevant repository on GitLab__
  - [EPUG repository][epug-gitlab]
  - [emblr repository][emblr-gitlab]
- __Prepare a list of links to further reading/related resources__, so participants can follow up on the short session in their own time. This may help you feel under less pressure to cover everything in the time available.

### During the session

Here we include and adapt the most relevant
points from "top ten tips for participatory live coding,"
[a section in The Carpentries Instructor Training][cit-participatory-live-coding] curriculum.
We leave out the tips that are less relevant for a user group/short demo session.

- __Go slowly.__ For every command you type, every word of code you write, every menu item or website button you click, say out loud what you are doing while you do it. Then point to the command and its output on the screen and go through it a second time. This slows you down and allows learners to copy what you do, or to catch up. Do not copy-paste code.
- __Use your screen wisely.__ Use a big font, and maximise the window. A black font on a white background works better than a light font on a dark background. When the bottom of the projector screen is at the same height, or below, the heads of the learners, people in the back won’t be able to see the lower parts. Draw up the bottom of your window(s) to compensate. Pay attention to the lighting (not too dark, no lights directly on/above the presenter’s screen) and if needed, re-position the tables so all learners can see the screen, and helpers can easily reach all learners.
- __Turn off notifications__ on your laptop and phone.
- __Stick to the lesson material.__ (In this case, we mean the notes and examples you prepared in advance.) Some instructors use printouts of the lesson material during teaching. Others use a second device (tablet or laptop) when teaching, on which they can view their notes and the Etherpad session. This seems to be more reliable than displaying one virtual desktop while flipping back and forth to another.
- __Embrace mistakes.__ No matter how well prepared you are, you will make mistakes. This is OK! Use the opportunity to help others understand what went wrong. Try to get into the habit of talking through your thought process as you fix the error/try to figure out where the bug is. Don't be afraid to ask your audience for help - they will often be faster to spot your typo than you are, and you can expect them to be kind while they're helping you.

Note that the relevance of some of these points varies depending on the target audience of the session e.g. if your session is aimed at those with a lot of previous experience with the language, you can probably move a little faster through parts of the material. You should still take time when introducing new concepts, though - _don't assume that everyone knows everything you know_.

In addition to the points from The Carpentries above, here are a few more
things to do during the session:

- __Be kind.__ These sessions aren't an opportunity for you to demonstrate how much cleverer you are than everyone else. You can expect participants with a range of different experience levels and backgrounds in whatever you're demonstrating - they don't necessarily have to understand everything you cover but they should still feel welcome and comfortable enough to ask questions.
- __Finish on time.__ Expect people to have other places to be after the scheduled time has expired. Some participants may want to stick around for further discussion, but you should try to make sure that you don't cover any new material after this point. If you do, it's even more important than usual that you post the material online, so people can catch up when they've got more time.

### After the session

- [ ] Polish the material you prepared, so it's ready to be re-used (by you or someone else)
  - add additional content based on what was discussed during the session
  - fix any errors/typos you spotted during the session
- [ ] Add material from the session to the relevant repository on GitLab, if you didn't already do so (contact the organisers if you're not sure how to do this)
  - [EPUG repository][epug-gitlab]
  - [emblr repository][emblr-gitlab]
- [ ] Post a link to your material on the relevant [chat.embl.org][embl-chat] channel
  - [Python channel][embl-chat-python]
  - [R & statistics channel][embl-chat-r]

## Recommended further reading

If you're interested in learning more good practices for teaching programming,
check out the resources below:

- [Ten tips for preparing a short coding demo/tutorial](https://git.embl.de/grp-bio-it/python-user-group-2020/-/blob/master/session_5_preparing_an_EPUG_session/session-lead-advice.html) slides by Toby Hodges
- [Teaching Tech Together][ttt] by Greg Wilson
- [The Carpentries Instructor Training][cit-homepage]

## Acknowledgments

The following people contributed advice and suggestions via Twitter
while we were creating this document:

- Dr. Melissa Burke / EMBL-EBI / [@burkemlou](https://twitter.com/burkemlou)
- Hao Ye / University of Florida / [@Hao_and_Y](https://twitter.com/Hao_and_Y)
- Florian Huber / EMBL / [@TheRealFloHu](https://twitter.com/TheRealFloHu)

{% include links.md %}
