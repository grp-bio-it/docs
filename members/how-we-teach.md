---
title: How we teach
layout: page
permalink: /members/how-we-teach/
parent: For Community Members
tags: []
---

**Note: familiarity with all of the above concepts is not a requirement
to teach on a Bio-IT course -
we welcome and encourage any community member
who wants to get involved!**

Bio-IT courses/workshops are typically designed to
maximise the time learners/participants have to apply what they're learning.
Many of [our instructors]({{site.baseurl}}/members/instructors/) are certified with
[The Carpentries][carpentries] and,
inspired by their
[Instructor Training Curriculum][cit-homepage],
we try to organise and run our training events "in the Carpentries style."
Specifically:

- we aim to provide attendees with opportunities for
[guided practice][cit-guided-practice]
while they learn.
  - Provide plenty of exercises and opportunities for [formative assessment][cit-formative-assesment].
  - Teach with [participatory live coding][cit-participatory-live-coding] where possible
- using [coloured sticky notes][cit-sticky-notes] to monitor progress and collect feedback throughout the course/workshop
- have at least two instructors for every workshop (["never teach alone"][cit-never-teach-alone]) and helpers, to support learners during taught sessions

If you'd like to learn more about the
evidence-based teaching practices mentioned above,
we recommend reading the Carpentries Instructor Training curriculum,
which is [available in full online][cit-homepage].
[Greg Wilson][gwilson-website]'s book,
[Teaching Tech Together][ttt],
expands upon the same concepts and
is similarly available to read.

{% include links.md %}
