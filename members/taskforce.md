---
title: Bio-IT Taskforce
layout: page
permalink: /members/taskforce/
parent: For Community Members
tags: ["strategy", "gratitude"]
---

The Bio-IT Taskforce is a steering committee for the Bio-IT Project,
whose membership is drawn from the community itself.
Members tend to be key stakeholders in the project:
Bio-IT project coordinators,
staff providing computational support to researchers,
PhD students/post-docs/research staff keen on getting involved in community activity,
research group leaders with an interest and/or remit for bioinformatics support,
and members of the IT Services group.
The Taskforce originally formed around the group whose grassroots activity,
establishing a common library of Linux software tools for EMBL systems,
[led to the foundation of the Bio-IT Project]({{site.baseurl}}about/background/):
Jean-Karim Hériché,
Venkata Satagopam,
and Frank Thommen.

Taskforce meetings follow a regular bi-monthly schedule,
with occasional extraordinary meetings held when the need arises.
Meetings usually follow the [Lean Coffee][lean-coffee] format,
unless discussion is expected to centre around one or two topics only.
See [this checklist]({{site.baseurl}}how/taskforce-meeting/) for more
information about how meetings are organised/run.

{% include links.md %}
