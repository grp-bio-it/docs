---
title: EMBL Git Admins
layout: page
permalink: /members/git-admins/
parent: For Community Members
tags: []
---

The EMBL Git Admins are volunteers,
who administer and maintain the Bio-IT community
[GitLab][embl-gitlab] and [Mattermost Chat][embl-chat] platforms.
This work involves managing updates to these systems,
troubleshooting problems,
administering user accounts,
and managing containers and runners for GitLab-CI.

Current admins:

- Wasiu Akanni
- Toby Hodges
- Jelle Scholtalbers
- Jakob Wirbel

Former admins:

- Holger Dinkel*
- Grischa Toedt*
- Marc Gouw

\* Frank Thommen was the administrator of the community's previous
Git repository hosting system, based on [Gitolite][gitolite].
Grischa and Holger were responsible for setting up the GitLab system
and migrating existing repositories from Gitolite to GitLab.

{% include links.md %}
