---
title: Bio-IT Instructors
layout: page
permalink: /members/instructors/
parent: For Community Members
tags: []
---

Below is a list of all community members who have taught
at least one course or workshop organised via the Bio-IT Project.
The list is ordered alphabetically by last name.

- Wasiu Akanni
- Renato Alves
- Julian Bauer
- Luis Pedro Coelho
- Holger Dinkel
- Mattia Forneris
- Markus Fritz
- Marco Galardini
- Charles Girardot
- Marc Gouw
- Michael Hall
- Jonas Hartmann
- Matthias Helmling
- Jean-Karim Hériché
- Volker Hilsenstein
- Toby Hodges
- Florian Huber
- Nicolai Karcher
- Supriya Khedkar
- Bernd Klaus
- Anna Kreshuk
- Manjeet Kumar
- Dominik Kutra
- Benjamin Lang
- Adrien Leger
- Kimberly Meechan
- Gregor Mönke
- Josep Moscardó
- William Murphy
- Nikolaos Papadopoulos
- Jurij Pečar
- Tobias Rasse
- Karin Sasaki
- Jelle Scholtalbers
- Thomas Schwarzl
- Malvika Sharan
- Mike L Smith
- Frank Thommen
- Grischa Tödt
- Thea Van Rossum
- Georg Zeller

{% include links.md %}
