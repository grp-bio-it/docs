---
title: Home
layout: default
description: "Documentation of the EMBL Bio-IT Community."
permalink: /
nav_order: 0
---

# EMBL Bio-IT Community Documentation

Documentation of the [EMBL Bio-IT Project][bio-it-homepage].
Bio-IT was established to build and support the computational biology/bioinformatics
community at the [European Molecular Biology Laboratory (EMBL)][embl-homepage], Heidelberg, Germany.

These docs provide information and practical guides about the community, the project,
and their activities.
The site was originally developed by [Toby Hodges][toby-homepage]
under the [Center for Scientific Collaboration and Community Engagement's][cscce]
concept of a *Community Playbook*, as part of the
[CSCCE Community Engagement Fellows Program][cefp-homepage] 2019 (CEFP2019).

## How to use this documentation

All visitors may find our collection of [checklists]({{site.baseurl}}checklists/)
useful for planning, organising, and running events and meetings.

If you're new to the project/community,
you should check out the [About]({{site.baseurl}}about/) section
and the [Bio-IT Homepage][bio-it-homepage].
Those particularly interested in how Bio-IT does things
may also want to look at the information
[for Community Members]({{site.baseurl}}members/) too.

Community coordinators/community engagement managers should
read the material under [For Community Coordinators]({{site.baseurl}}coordination/),
which provides details of processes, tools, and decision-making
that underpin the coordination of the community and project.

## Contact

If you have questions about these pages,
about our community,
or about the EMBL Bio-IT Project in general,
please contact <a href="mailto:{{ site.email }}">{{ site.email }}</a>.

{% include full_toc.html %}

{% include links.md %}
