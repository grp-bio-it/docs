---
title: Project Background
layout: page
permalink: /about/background/
parent: About the Community
nav_order: 1
tags: ["strategy", "motivation"]
---

![Computational activity at EMBL-HD 2009-2018]({{site.baseurl}}images/All_Units_Time.png)

In recent years, computational research activity has been growing steadily at EMBL Heidelberg.
This increase in bioinformatics and computational biology activity reflects the
wider trend across the life sciences,
as research biology becomes increasingly data-driven.
However, this increase in research computing presents some challenges:
there is a need for training and support of researchers who must learn computational methods;
the diversity of methods and data types/formats require a wide range of different software and hardware;
there is a risk that researchers and support staff will waste time developing solutions and systems that already exist in other research groups/units;
and computational researchers and staff can sometimes be isolated in groups that otherwise use predominantly lab-based methods.
With these challenges in mind,
the Bio-IT Project was established in 2010,
with the aim to build and support a community of computational
biologists and bioinformaticians at the institute.

## Project History

![Timeline of the EMBL Bio-IT Project]({{site.baseurl}}images/Bio_IT_Timeline_Mar2019.png)

In its early stages,
the project built on the existing
[grassroots efforts of a group of computational staff]({{site.baseurl}}members/taskforce/)
who were already working collaboratively to establish a unified library of
Linux software tools available across the various different compute environments that existed at EMBL Heidelberg at the time.

Those who developed this resource,
and other members of the computational community across the research units at EMBL Heidelberg, began meeting regularly and the Bio-IT Taksforce was established.
The Taskforce acts as something like a "steering group" for the Project, meeting every two months to discuss issues relating to computational biology and bioinformatics at the institute and planning strategy to address these issues.

The years since the Project was established have seen many new resources and opportunities become available to computational scientists at EMBL, such as:

- computational training courses
- the EMBL GitLab system, [git.embl.de][embl-gitlab]
- weekly drop-in consulting sessions
- an information portal, [bio-it.embl.de][bio-it-homepage]
- fortnightly R & Python user group meetings
- an instant messaging system, [chat.embl.org][embl-chat]

The community has benefitted from many other important developments in addition to those listed above.

## Project Coordination

Project strategy and activity is largely discussed and planned
by the [Bio-IT Taskforce]({{site.baseurl}}members/taskforce/).
Within this group, the following people are responsible
for coordination of project activity:

- Dr Renato Alves & Dr Toby Hodges, community coordinators
- Dr Georg Zeller, Team Leader with mandate to provide bioinformatics services
- Dr Toby Gibson, Team Leader with mandate to provide bioinformatics training
- Dr Peer Bork, Strategic Head of Bioinformatics, EMBL Heidelberg

Most of the initial coordination work to establish the community and project was done by Dr Aidan Budd. Further work to expand the community and activity of the project was undertaken by Dr Malvika Sharan.

{% include links.md %}
