---
title: Bio-IT Mission
layout: page
permalink: /about/mission/
parent: About the Community
nav_order: 2
tags: ["strategy", "motivation"]
---

> The Bio-IT project was established with a mission to provide EMBL Heidelberg
> with a platform for __information and discussion about bioinformatics__
> and to __promote networking__ amongst its __diverse bioinformatics user community__.

We aim to achieve this mission through activity in four main areas:

- computational training & consulting
- community building
- development and maintenance of computational resources
- dissemination of information relevant to the community

{% include links.md %}
