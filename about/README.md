---
title: About the Community
layout: page
permalink: /about/
has_children: true
nav_order: 1
---

In this section,
we provide an overview of the community and the goals of the Bio-IT Project.
This is a good place to start if you're new to EMBL and/or the community project.
We include a set of *community personas*:
(fictionalised) archetypes of the different people who make up our community,
and information on how engaging with the community might benefit them.
