---
title: "Computational Activity at EMBL Heidelberg"
layout: page
permalink: /about/target-audience/
parent: About the Community
nav_order: 3
tags: ["metrics", "surveys"]
---

Every three years since 2009,
Bio-IT project coordinators have carried out a survey
of computational research activity at EMBL Heidelberg.
This survey is sent to all Group and Team Leaders (GTLs) at the institute,
asking them to estimate the proportion of research activity
that is primarily computational, within their group.
In more recent surveys, additional questions have been asked
in an attempt to assess the impact of the various support activities
(e.g. see [Usage & Demand of Bio-IT Services & Resources]({{site.baseurl}}coordination/strategy/service-metrics/)).

Project coordinators use the information collected
to try to understand the needs of the community,
to plan Bio-IT activity,
and in external and internal communications about the community.
The data collected,
along with summary figures and the scripts used to create them,
are stored in [this repository (internal access only)][bioinformatics-surveys-repo].

The results of the latest survey,
carried out in 2018,
showed that,
for the first time,
computational researchers
(defined as anyone whose research activity is >50% computational)
accounted for more than half of scientific members at EMBL Heidelberg.
The results indicated there were ~250 such computational researchers
at the institute at the time of the survey.
We consider these researchers to be the target audience for the Bio-IT Project,
with the aim that they engage with our activities
and with the rest of the community,
so that we can better support them in their work
while other community members can tap into their expertise and experience.

![Computational activity at EMBL-HD 2009-2018]({{site.baseurl}}images/Activity_By_Group_2018.png)

{% include links.md %}
