---
title: Community Personas
description: Profiles of the different types of persona that make up the Bio-IT community.
layout: page
permalink: /about/personas/
parent: About the Community
nav_order: 4
tags: ["motivation"]
---

Personas (fictional) representing the different categories that most members of the Bio-IT community can be divided into.
These personas help us to develop our community strategy
and check that project activity is supporting all members.
In future, we hope to tag content in these docs
with the personas that it is relevant to.

The use of the classifications,
novice, competent practitioner, and expert,
is based on that used by [The Carpentries][carpentries-three-stages]
to inform their lesson design and teaching.
In practice, it can be difficult to define exactly to which of these categories
any given community member belongs.
Here we separate experts from novices and competent practitioners
because the kind of support that the Bio-IT Project provides
to those with an advanced understanding of
computational techniques, tools, and resources,
differs considerably to that provided to those
who are still getting to grips with these things.

-------------------------------------------------------------------------------

## Computational Scientific Support Staff

#### Personal Details

- Strong technical background
- May have a background in biology/bioinformatics or from a different computational background

#### Bio

A PhD in bioinformatics was enough experience for __Marco__ to know that he didn't
want to pursue a career in academic research.
He enjoys the problem-solving and creative aspects of
developing tools and pipelines to answer biological research questions but
isn't excited by other aspects of research such as
writing and reading papers,
dedication to one or a small handful of projects,
applying for grants, etc.

Instead, Marco prefers to help other people with their projects,
providing advice and training,
and developing new tools and resources to support
the work of scientists in their unit.
Marco enjoys his work and the freedom of the academic environment
but worries that there is no clear career path
for people like him.

_The Bio-IT Project can help Marco by
connecting him with other support staff in a similar position,
to share knowledge and develop solutions to common problems together,
and by providing relevant training to the scientists that Marco supports._

#### Behaviours

- Keep "normal" working hours
- Work on multiple projects
- Engage with Bio-IT project strategy, infrastructure discussion
- May maintain/develop resources for community
- Broad knowledge of technologies, software used in their discipline
- Occasional administration of research compute systems and software

#### Needs/Goals

- Goals shift according to demands of the scientists that they support
- Avoid wasting time by doing unnecessary/repetitive things that could be automated
- Provide tools and resources for scientists to do better research
- Easy prototyping/development of new tools/systems

#### Motivations

- Likes to help people
- Enjoys a variety of projects to work on
- Enjoys problem-solving

-------------------------------------------------------------------------------

## IT Support Staff

#### Personal Details

- Strong technical background
- Little biology/bioinformatics knowledge
- Has to juggle many different tasks/requests

#### Bio

__Marcel__ is a trained systems engineer
who now administers compute resources and environments at EMBL,
as a member of the IT Services group.
With no background in biology,
he sometimes finds it challenging to understand
the needs and requests of the users of these systems,
and the function of the software that they want to run.
Similarly, Marcel often finds that the researchers that use these systems
have only a limited understanding of the architecture and potential of the
compute infrastructure that's available to them.

However, supporting researchers means working to tackle an ever-changing range
of different challenges
and Marcel likes the variety that this brings to his work.
He's also pleased to see that the scientists are increasingly using
tools to manage their compute environments and workflows.
He'd like to help these researchers continue to make better use
of the resources that are available to them at EMBL,
and hear more from them about the things that they expect to need in the future,
so that he can plan his activities more strategically.

_The Bio-IT Project can help Marcel by
providing a platform for dialogue to between researchers and IT support staff,
enabling him to train scientists to make effective use of the resources available,
and encouraging computational scientists to support each other._

#### Behaviours

- Engages with Bio-IT Project strategy
- Develops new resources and systems for the community
- Answers questions relating to technical topics
- Shares resources (blogposts, videos, etc) about new technologies and software

#### Needs/Goals

- Enable researchers to do things that would be beyond their own capabilities
- Maintain/develop stable and secure compute infrastructure
- Avoid dealing with repetitive support requests from researchers
- Understand the needs of researchers

#### Motivations

- Gets satisfaction from engineering solutions to scientists' problems
- Enjoys seeing powerful compute resources used effectively
- Enjoys working with cutting (sometimes bleeding) edge technologies and tools

-------------------------------------------------------------------------------

## PhD Student - Expert in Computational Techniques

#### Personal Details

- already studied bioinformatics, computer science, or some other computational subject before starting their PhD project
- works on some methodological project, probably developing a new computational approach or software associated with a novel technology

#### Bio

__Melissa__ learned to program as a hobby while she was at school.
A Masters in molecular biology provided the first opportunity
for her to apply these skills in the analysis of biological data.
She's now in the second year of a PhD project,
using machine learning approaches to detect structural differences in
fluorescence-microscopy images of developing embryos.

Melissa's got pretty good at teaching herself
and quickly picking up new tools and techniques that she needs for her research.
To complete her research,
Melissa needs to make heavy use of the EMBL compute cluster,
including GPU resources.
She's also keen to share her knowledge and enthusiasm for programming
with her peers, often helping her lab mates
and fellow PhD students with their data analysis questions.

_Bio-IT can help Melissa by
making it easier for her to communicate with the administrators
and other users of HPC/GPU resources,
enabling her to gain teaching experience by sharing her knowledge with others,
and connecting her with other keen programmers at EMBL._

#### Behaviours

- push the limits of what can be done with the compute resources available locally
- develop new software and pipelines for their research project
- often support the computational work of their lab-mates
- engage with the more technical Bio-IT discussions and events
- may get involved with teaching and/or running sessions at programming group meetings/coding club sessions
- teach themselves new things in absence of advanced training
- Get most of their information from outside Bio-IT resources, as they work on "edge cases"

#### Needs/Goals

- complete and publish research project(s)
- resources and support for sophisticated software development projects
- strategies and tools for managing a large amount of data

#### Motivations

- interested in applying their technical skills to biological questions
- know/work with people who share their interest in coding and technical topics
- enjoys learning new to use new tools, programming languages, etc

-------------------------------------------------------------------------------

## PhD Student - Novice/Competent Practitioner in Computational Techiniques

#### Personal Details

- education in biology-related discipline
- no/very little previous bioinformatics experience
- needs to pick up some computational skills for analysis of data produced in their project

#### Bio

__Mohamed__ excelled in his undergraduate and Masters studies
in cellular biology and has now begun a PhD project at EMBL.
His project involves the production of a large amount of imaging data
that he now needs to learn how to manage and analyse.

He knows that learning to program will be beneficial to his research career
but he's interested in getting results,
not so much in the technical details
of the langauges and systems that he has to use.
Most of what he wants to do right now is
reproducing analyses in the papers that he reads,
but he expects to need to write some of his own code
to perform the exact analyses and data visualisation figures that he will need for his thesis.
He can get some guidance from his supervisor
and the one computational biologist post-doc in his group,
but they are both very busy
and often aren't available to answer Mohamed's questions.

_Bio-IT can help Mohamed by providing him with training
to run the analyses and produce the figures that he needs.
Rather than wait for a course that he needs,
he might follow the material on his own.
Bio-IT also provides him with ways to get to know
other people at EMBL with whom he can discuss what he needs to do
and guidance in best practices that will make his computational science
more robust and reproducible in the long term._

#### Behaviours

- attends beginner-level Bio-IT courses
- asks questions of their lab-mates, or on chat.embl.org, or at a drop-in session
- looks for information
- pursues "good enough" practices in order to get results

#### Needs/Goals

- complete and publish research project(s)
- learn enough programming to be able to analyse data and produce publication-ready visualisations

#### Motivations

- learn skills that will help with a career in research
- cares that something works, not whether it's optimised

-------------------------------------------------------------------------------

## PostDoc - Expert in Computational Techniques

#### Personal Details

- combined expertise of computational approaches and domain of research
- supervises students and research assistants who may need to learn more computing skills

#### Bio

__May__ completed a PhD project on protein-protein interactions
in a recently-discovered cellular signalling pathway
and has spent the years since working on
mutliple projects to follow up some of the findings of that project.
These projects involve multiple different types of experiment and data,
which she wants to integrate together to maximise the novelty and impact
of the subsequent publications.

May has an encyclopedic knowledge of her research area
and can easy develop the new pipelines and
small pieces of software
that she needs to analyse her results.
However,
she must divide her time between
these projects,
writing papers and applications
for grants and group leader positions,
and supervising students and staff.

_Bio-IT can help May by
providing training
and information
for the members of her research projects,
and by providing resources
that help her to get her work done as quickly and easily as possible._

#### Behaviours

- only interacts with Bio-IT when something isn't working
- influences the way that other people in their group do things
- pushes the limits of computational resources available at EMBL

#### Needs/Goals

- Complete and publish research project(s)
- Access best available software and hardware for their research
- Gain understanding of technical requirements for their own lab

#### Motivations

- Maximise quality and impact of their research
- Ensure their students/collaborators have the skills and knowledge necessary to perform the research

-------------------------------------------------------------------------------

## PostDoc - Novice/Competent Practitioner in Computational Techiniques

#### Personal Details

- In-depth knowledge of their research domain
- Needs to learn some computing skills to complete their research
- May be first and only person in research group using computational techniques

#### Bio

__Maria__ is leading a project investigating
the role of structural variants of the protein Jsr-9 in cancer metastasis.
The study revolves around single-cell transcriptomic analysis of
metastasised tumours.
Although she picked up some basic knowledge of R during her PhD,
this is the first encounter that Maria has had with large-scale,
programmatic data analysis.

She is responsible for the data analysis on the project
and wants to get from raw data to results as quickly as possible,
which will inform the follow-up experiments that she
and her colleagues will perform in the lab.

_The Bio-IT Project can help Maria by
orienting her with computational resources
and software available to aid her data analysis,
and by connecting her with other community members
who have experience with this kind of analysis._

#### Behaviours

- attends Bio-IT courses
- looks for information
- pursues "good enough" practices in order to get results

#### Needs/Goals

- complete and publish research project(s)
- learn enough programming to be able to analyse data and produce publication-ready visualisations

#### Motivations

- learn skills that will help with a career in research
- cares that something works, not whether it's optimised

-------------------------------------------------------------------------------

## Possible other personas?

- short-term visitor
- software developer
- can we combine novice/CP PhD & Post-Doc together?

{% include links.md %}
